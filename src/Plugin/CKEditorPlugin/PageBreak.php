<?php

namespace Drupal\pagebreak\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "pagebreak" plugin.
 *
 * @CKEditorPlugin(
 *   id = "pagebreak",
 *   label = @Translation("PageBreak"),
 * )
 */
class PageBreak extends CKEditorPluginBase {

  /**
   * Get libraries path to plugin.
   *
   * @return string
   *   Returns the path.
   */
  private function getLibrariesPath() {
    $libraries_path = 'libraries/pagebreak';
    if (\Drupal::hasService('library.libraries_directory_file_finder')) {
      /** @var \Drupal\Core\Asset\LibrariesDirectoryFileFinder $library_file_finder */
      $library_file_finder = \Drupal::service('library.libraries_directory_file_finder');
      // Check primary/default path.
      $libraries_path = $library_file_finder->find('pagebreak');
      if (!$libraries_path) {
        // Check ckeditor secondary path.
        $libraries_path = $library_file_finder->find('ckeditor/plugins/pagebreak');
      }
    }
    elseif (function_exists('libraries_get_path')) {
      // Check primary/default path.
      $libraries_path = libraries_get_path('pagebreak');
      if (!$libraries_path) {
        // Check ckeditor secondary path.
        $libraries_path = libraries_get_path('ckeditor/plugins/pagebreak');
      }
    }
    return $libraries_path;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->getLibrariesPath() . '/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return ['fakeobjects'];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'PageBreak' => [
        'label' => $this->t('PageBreak'),
        'image' => $this->getLibrariesPath() . '/icons/pagebreak.png',
      ],
    ];
  }

}
