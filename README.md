## CKEditor PageBreak

This module provides integration with the PageBreak plugin for CKEditor.

This plugin adds toolbar button which inserts horizontal page breaks.
This feature is useful for setting document printing sections.

Text entered in a browser-based text editor such as CKEditor differs from word
processors because it is not divided into physical pages. In effect, this
plugin lets you define pages.

### Requirements

* Core CKEditor module
* Pagebreak plugin - https://ckeditor.com/cke4/addon/pagebreak
* FakeObjects module - https://www.drupal.org/project/fakeobjects
* FakeObjects plugin - https://ckeditor.com/cke4/addon/fakeobjects

### Install

Best way to install module is through composer. See below:

```bash
composer require drupal/pagebreak
```

1. Download the plugin from http://ckeditor.com/addon/pagebreak, at least
version 4.5.11.

2. Place the plugin in the root libraries folder (_/libraries/pagebreak/_).
Should look like **/libraries/pagebreak/plugin.js** when the plugin is uploaded
to this folder.

3. Enable PageBreak module.

4. Drag the new PageBreak button into your toolbar for the format of choice
(only tested under Full HTML so far). See the following page:
**/admin/config/content/formats/manage/full_html**.

6. Create some content, and within the editor, click the button for where you
want to insert a page break for print.


### Usage

A new icon will appear in your toolbar, drag it into your active toolbar to enable
it. Simply place your cursor where you want it in the editor window, and click to
insert a page break. Each page break will cause print setups to stop and create a
new page. You can test this by creating a dummy page, filling it with content, adding
some page breaks and going to File > Print and checking the preview.

### Maintainers

* George Anderson - https://www.drupal.org/u/geoanders
